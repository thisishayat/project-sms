<?php
namespace Freemig\Profile\Repositories;
use Freemig\Profile\Models\Profile;
use Freemig\Profile\Repositories\Contracts\TokenContract;
use Freemig\Profile\Repositories\Contracts\UserContract;
use Freemig\Profile\Models\User as User;
use Freemig\Social\Models\Friend;
use Freemig\Social\Models\FriendGroup;
use Freemig\Social\Models\FriendRequest;
use Freemig\Profile\Models\Token as Token;
use Freemig\Social\Repositories\FriendRepository;

class TokenRepository implements TokenContract
{
    private $tokenModel;

    function __construct(Token $token)
    {
        $this->tokenModel = $token;
    }


    public function createToken($params)
    {
        $this->tokenModel->user_id = $params['user_id'];
        $this->tokenModel->token = $params['token'];
        $this->tokenModel->expiry_time = $params['expiry_time'];
        $this->tokenModel->type = $params['type'];
        $this->tokenModel->save();
        return true;
    }
    public function tokenExist($token){
        return $this->tokenModel->where('token','=',$token)->first();
    }

    public function getUserActivationToken($userId){
        //return $this->tokenModel->where('user_id','=',$userId)->where('expiry_time','>',\Carbon\Carbon::now()->subHours(4))->first();
        return $this->tokenModel->where('user_id','=',$userId)->where('type','1')->first();
    }

    public function isTokenActive($userId,$token){

        $tokenData = $this->tokenModel->where('user_id','=',$userId)->where('token', $token)
            ->where('expiry_time','>',\Carbon\Carbon::now())->first();

        return (count($tokenData) > 0) ? true : false;
    }

    public function isTokenActiveByUserId($userId) {
        $tokenData = $this->tokenModel->where('user_id','=',$userId)->get();
        //-->last()

        //dump($tokenData, \Carbon\Carbon::now());
        return (!$tokenData->isEmpty() && $tokenData->last()->expiry_time > \Carbon\Carbon::now()) ? true : false;
    }

    public function removeUserActivationToken($userId, $token) { //dd($userId, $token);
        $d = $this->tokenModel->where(function($qry) use($userId, $token) {
            $qry->where('user_id', $userId);
            $qry->where('token', $token);
            $qry->where('type', '1');
        })->delete();
     //   dd($d->toSql());
        return $d;
    }
}