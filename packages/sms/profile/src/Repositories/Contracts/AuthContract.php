<?php
namespace Freemig\Profile\Repositories\Contracts;

interface AuthContract
{
    public function login($params);
    public function logout();
    public function resetPassword($email);
    public function isAuthenticated();

}