<?php
namespace Freemig\Profile\Repositories\Contracts;

interface UserContract
{
    public function createUser($params);
    public function updateUser($updateObject=[], $userId);
    public function deleteUser($userId);
    public function getUserByEmail($email);
    public function getUserById($userId);
    public function activeAccountByUserId($userId);
    public function updatePassword($userId,$password);

}