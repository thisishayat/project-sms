<?php
namespace Freemig\Profile\Repositories\Contracts;

interface TokenContract
{
    public function createToken($params);
    public function tokenExist($token);
}