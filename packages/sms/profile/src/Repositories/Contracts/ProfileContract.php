<?php
namespace Freemig\Profile\Repositories\Contracts;

interface ProfileContract
{
    public function createProfile($params);
    public function editProfile($updatedParams, $profileId);
    public function getProfile($userId);
}