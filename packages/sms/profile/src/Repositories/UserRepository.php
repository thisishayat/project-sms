<?php
namespace Freemig\Profile\Repositories;
use Freemig\Profile\Models\Profile;
use Freemig\Profile\Repositories\Contracts\UserContract;
use Freemig\Profile\Models\User as User;
use Freemig\Search\Facades\Search;
use Freemig\Profile\Repositories\ProfileRepository;
use Freemig\Social\Repositories\FriendRepository;

class UserRepository implements UserContract
{
    private $userModel;
    private $profileRepo;

    function __construct(User $user)
    {
        $this->userModel = $user;
    }


    public function createUser($params){ //dd($params, array_has($params,'dialCode'));
        $this->userModel->first_name = $params['first_name'];
        $this->userModel->last_name = $params['last_name'];
        $this->userModel->email = $params['email'];
        $this->userModel->username = $params['username'];
        $this->userModel->password = \Hash::make($params['password']);
        $this->userModel->gender = $params['gender'];
        $this->userModel->country = $params['country'];
        $this->userModel->user_type = $params['user_type'];
        $this->userModel->phone = $params['phone'];
        $this->userModel->dob = $params['dob'];
        $this->userModel->org_name = $params['org_name'];
        $this->userModel->expertise = $params['expertise'];
        $this->userModel->area = $params['area'];
        $this->userModel->work_region = $params['work_region'];
        $this->userModel->work_region_zip = $params['work_region_zip'];
        $this->userModel->profile_avatar = $params['profile_avatar'];

        $this->userModel->last_login = date('Y-m-d H:i:s');
        $this->userModel->registration_date = date('Y-m-d H:i:s');
        $this->userModel->is_active = 0;
        $this->userModel->is_disable = 0;
        $this->userModel->is_remove = 0;
        $user = $this->userModel->save();
        $indexer = new Search();
        $indexer->indexUser($this->userModel->id);
        return ['userId'=>$this->userModel->id,'username'=>$this->userModel->username];
    }
    public function updateUser($updates = [], $userId){
        $this->userModel->find($userId)->update($updates);
        return true;
    }
    public function deleteUser($userId){

    }
    public function getUserByEmail($email){
        return $this->userModel->where('email','=',$email)->first()->toArray();
    }
    public function getUserById($userId){
        return $this->userModel->where('id','=',$userId)->first();
    }
    public function getUserByIdPage($userId){
        return $this->userModel->where('id','=',$userId)->first()->toarray();
    }
    public function getUserByUsername($username){

        if($this->userModel->count()>0)
        {

            $user = $this->userModel->where('username',$username)->get();
            return (!$user->isEmpty()) ? $user[0]->toArray() : [];
        }
        return false;
    }
    public function getUserByIdMinimum($userId, $ref_id=null,$type='user'){

        if($ref_id == null){
            $ref_id=\Session::get('userId');
        }

        $this->profileRepo = new \Freemig\Profile\Repositories\ProfileRepository(new Profile());
        $userInfo = $this->getUserById($userId);
        $profileInfo = $this->profileRepo->getProfileMinimum($userId,$ref_id);
        $avatar = $userInfo['profile_avatar'];
        $data = array(
            'id' => $userInfo['id'],
            'username' => $userInfo['username'],
            'full_name'=> $userInfo['first_name'].' '.$userInfo['last_name'],
            'avatar'=> $avatar,
            'cover'=> $profileInfo['cover_photo'],
            'url'=> '/'.$userInfo['username'],
            'edit_profile'=>'/profile/edit',
            'fr_status'=> $profileInfo['fr_status'],
            'status_creator'=> $profileInfo['status_creator'],
            'fr_privacy'=> $profileInfo['fr_privacy'],
            'follow_status'=> $profileInfo['follow_status'],
            'workTitle'=> $profileInfo['workTitle'],
            'country'=> isset($profileInfo['country']['v'])?$profileInfo['country']['v']:'',
        );
        return $data;
    }
    public function activeAccountByUserId($userId){
        $this->userModel->where('id','=',$userId)->update(['is_active' => 1]);
        return true;
    }
    public function updatePassword($userId,$password)
    {
        $this->userModel->where('id','=',$userId)->update(['password' => \Hash::make($password)]);
        return true;
    }
    public function getUserInfoByEmail($email){
        return $this->userModel->where('email','=',$email)->first();
    }
    public function getEmailExist($email){
        $info = $this->userModel->where('email', '=' ,$email)->first();
        if(count($info)>0){
            return true;
        } else {
            return false;
        }
    }
    public function getUsernameExist($username){
        $info = $this->userModel->where('username', '=' ,$username)->first();
        if(count($info)>0){
            return true;
        } else {
            return false;
        }
    }

    public function isUserActiveById($userId) {
        $user = $this->userModel->where('id', '=' ,$userId)->where('is_active','=',1)->get();
        if(!$user->isEmpty()) {
            return true;
        }
        return false;
    }

    public function isUserActiveByEmail($email) {
        $user = $this->userModel->where('email', '=' ,$email)->where('is_active','=',1)->get();
        if(!$user->isEmpty()) {
            return true;
        }
        return false;
    }

    public function getAllUsers()
    {
        return $this->userModel->all()->toArray();
    }

    public function getProfessionalUsers()
    {
        return $this->userModel->where('user_type', '2')->get()->toArray();
    }

    public function getProfessionalUserIds($id=null)
    {
        if(is_null($id)) {
            return $this->userModel->where('user_type', '2')->get(['id']);
        }
        else {
            return $this->userModel->where('user_type', '2')->where('id', '!=', $id)->get(['id']);
        }
    }

    public function isProfessionalUser($userId) {
        $get = $this->userModel->where(['id'=>$userId, 'user_type'=>'2'])->get();
        if(!$get->isEmpty()) { return true; }
        return false;
    }
}
