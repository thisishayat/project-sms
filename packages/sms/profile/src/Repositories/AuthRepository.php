<?php
namespace Freemig\Profile\Repositories;
use Freemig\Profile\Repositories\Contracts\AuthContract;
use Freemig\Profile\Repositories\Contracts\UserContract;
use Freemig\Profile\Models\User as User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
class AuthRepository
{
    private $userModel;

    function __construct(User $user)
    {
        $this->userModel = $user;
    }
    public function login($params,$req) {

        $user = $this->userModel->where('username','=',$params['username'])
            ->where('password','=',md5($params['password']))->get()->toArray();

        if(count($user)>0) {
           $req->session()->set('isAuth', true);
            $req->session()->set('userId', $user[0]['id']);
            $req->session()->set('username', $user[0]['username']);
            return             $req->session()->get('userId');
        }
        return false;


    }
    public function isAuthenticated() {
//        return ['userId'=>1,'userName'=>'rhtythm'];
        if(\Session::get('isAuth'))
        {
            //return ['userId'=>\Session::get('userId'),'userName'=>\Session::get('userName')];
            return ['userId'=>1,'userName'=>\Session::get('userName')];
        } else {
            return ['userId'=>0,'userName'=>0];
        }
        return false;
    }
    public function logout() {
        $this->killSessionKeys(['isAuth','userId','username']);
        return true;

    }
    private function killSessionKeys($params)
    {
        foreach ($params as $v)
        {
            \Session::forget($v);
        }
    }
    public function resetPassword($email)
    {

    }

}
