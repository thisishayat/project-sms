<?php
namespace Freemig\Profile\Repositories;
use Freemig\Profile\Models\Profile;
use Freemig\Profile\Repositories\Contracts\ProfileContract;
use Freemig\Profile\Repositories\Contracts\UserContract;
use Freemig\Profile\Models\User as User;
use Freemig\Social\Models\Friend;
use Freemig\Social\Models\FriendGroup;
use Freemig\Social\Models\FriendRequest;
use Freemig\Social\Models\PhotoAlbum;
use Freemig\Social\Repositories\FriendRepository;
use Freemig\Profile\Repositories\UserRepository;
use Freemig\Notification\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Freemig\Profile\Models\Expertise;
use Freemig\Profile\Models\ExpertiseArea;
use Freemig\Search\Facades\Search;

class ProfileRepository
{
    private $profileModel;
    private $userRepo;
    private $alert;

    private $photoAlbum;
    private $search;

    function __construct(Profile $profile)
    {
        $this->profileModel = $profile;
        $this->userRepo = new UserRepository(new User());
        $this->alert = new Notification();
        $this->photoAlbum = new PhotoAlbum();
        $this->search = new Search();
    }

    public function createProfile($params)
    {

        $userId = $params['user_id'];
        $this->profileModel = new Profile();
        $this->profileModel->user_id = $params['user_id'];
        $this->profileModel->key = $params['key'];
        $this->profileModel->value = $params['value'];
        $this->profileModel->authorization = $params['authorization'];
        $user = $this->profileModel->save();
        return true;
    }
    public function editProfile($updatedParams, $profileId){
        if(count($updatedParams)>0 && $profileId>0) {
            $profile = $this->profileModel->find($profileId);
            $profile->update($updatedParams);
            return true;
        }
        return false;
    }
    public function  getProfileByKeys($userId, $keys = array()) {
        $profile = $this->profileModel
            ->where('user_id','=',$userId)
            ->where(function($where) use($userId, $keys) {
                $where->orWhere('key', '=', $keys[0]); // work
                $where->orWhere('key', '=', $keys[1]); // cover pic
                $where->orWhere('key', '=', $keys[2]); // country
            })
            ->select(['key', 'value', 'attributes'])
            ->get();

        return $profile;
    }

    public function  getProfile($userId){

        $profile = $this->profileModel->where('user_id','=',$userId)->get();
        //print_r($profile->toArray());exit();
        if($profile!=null) {
            foreach ($profile as $k => $v) {
                if ($v->key == 'work_region') {
                    $rv['work_region'][] = ['i' => $v->id, 'v' => $v->value, 'a' => $v->authorization, 'attr' => $v->attributes];
                } else if ($v->key == 'work') {
                    $rv['work'][] = ['i' => $v->id, 'v' => $v->value, 'a' => $v->authorization, 'attr' => $v->attributes];
                } elseif ($v->key == 'education') {
                    $rv['education'][] = ['i' => $v->id, 'v' => $v->value, 'a' => $v->authorization, 'attr' => $v->attributes];
                } else if($v->key =='profile_avatar') {
                    $value = '';
                    if($v->value == '' || is_null($v->value)) { $value = 'false.jpg';}
                    else { $value = $v->value;}
                    $rv['profileAvatar'] = ['i' => $v->id, 'v' => $value, 'a' => $v->authorization, 'attr' => $v->attributes];
                } else if($v->key == "expertise" && $v->value!="") {
                    $expertise = $this->getExpertise($v->value);
                    $rv['expertise'][] = ['i' => $v->id, 'v' => $v->value, 'a' => $v->authorization, 'attr' => $expertise->title];
                } else if($v->key=="area" && $v->value!="" && $v->value!=0) {
                    $expertiseArea = $this->getExpertiseArea(null,$v->value);
                    $rv['area'][] = ['i' => $v->id, 'v' => $v->value, 'a' => $v->authorization, 'attr' => $expertiseArea->title];
                } else if($v->key=="designation" && $v->value!="") {
                    $rv['designation'] = ['i' => $v->id, 'v' => $v->value, 'a' => $v->authorization, 'attr' => ''];
                } else {
                    $rv[$v['key']] = ['i' => $v->id, 'v' => $v->value, 'a' => $v->authorization, 'attr' => $v->attributes];
                }
            }

            $friends = new FriendRepository(new Friend(),new FriendRequest(), new FriendGroup());
            $rv['fr_privacy'] = $friends->userRelation(Session::get('userId'),$userId);
            $rv['fr_status'] = $friends->isFriendRequestPending($userId,Session::get('userId'));
            $rv['follow_status'] = $friends->isFollowing($userId);
            $rv['status_creator'] = $friends->isStatusCreator($userId);
            

             // friends, connections code moved to socialcontroller -> getFriendsForProfile()

            unset($rv['password']);
            unset($rv['password_confirmation']);
            unset($rv['_token']);
            return $rv;
        }
        return [];
    }
    // upcoming birthday
    public function  getDobProfile($userId){
        $month = (int)date('m');
        $today = (int)date('d');
        $limit = $today+7;
        $profile = $this->profileModel->where('user_id','=',$userId)->get();
        if($profile!=null) {
            foreach ($profile as $k => $v) {
                $rv[$v['key']] = ['v' => $v->value];
            }
            $checkMonth = (int)date('m',strtotime($rv['dob']['v']));
            $checkDate = (int)date('d',strtotime($rv['dob']['v']));

            if($month==$checkMonth && $today<$checkDate && $limit>=$checkDate){
                return $rv;
            } else {
                return [];
            }
        }
        return [];
    }
    // today birthday
    public function  getDobTodayProfile($userId){
        $month = (int)date('m');
        $today = (int)date('d');
        $limit = $today;
        $profile = $this->profileModel->where('user_id','=',$userId)->get();
        if($profile!=null) {
            foreach ($profile as $k => $v) {
                $rv[$v['key']] = ['v' => $v->value];
            }
            $checkMonth = (int)date('m',strtotime($rv['dob']['v']));
            $checkDate = (int)date('d',strtotime($rv['dob']['v']));
            if($month==$checkMonth && $today==$checkDate){
                return $rv;
            } else {
                return [];
            }
        }
        return [];
    }
    public function deleteProfile($userId,$key)
    {
        $this->profileModel = new Profile();
        $this->profileModel->where('user_id','=',$userId)->where('key','=',$key)->delete();
        return true;
    }
    public function setProfile($userId,$key,$value,$auth,$attributes=[]) {

        if($key!='work' && $key!='education') {
            $this->deleteProfile($userId,$key);
            unset($attributes['id']);
            foreach ($attributes as $k=>$v)
            {
                if($k!='attributes') {
                    unset($k);
                }
            }
        }

        $this->profileModel = new Profile();
        $this->profileModel->user_id = $userId;
        $this->profileModel->key = $key;
        $this->profileModel->value = $value;
        $this->profileModel->authorization = $auth;
        $this->profileModel->attributes = json_encode($attributes);
        $user = $this->profileModel->save();

        $this->search->indexUser($userId);
    }

    public function getProfileToolTipinfo($userName){
        $info = $this->profileModel->where('username','=',$userName)->get();
        return $info;
    }
    public function getProfileMinimum($userId,$ref_id=null){
        if($ref_id == null) {
            $ref_id=\Session::get('userId');
        }
        $info = $this->getProfileByKeys($userId, ['work','cover_pic','country']);
        $profileInfo = (!$info->isEmpty()) ? $info->toArray() : [];

        $workTitle = '';
        $country = '';
        $cover_photo = '';
        foreach($profileInfo as $p) {
            if ($p['key'] == 'work') {
                $work = json_decode($p['attributes']);
                $workTitle = $work->title;
            }
            if ($p['key'] == 'cover_pic') {
                $cover_photo = $p['value'];
            }
            if($p['key']=='country') {
                $country = $p['value'];
            }
        }
        /*$profileInfo = $this->getProfile($userId);
        if(isset($profileInfo['work']) && isset($profileInfo['work'][0]['attr'])){
            $work = json_decode($profileInfo['work'][0]['attr']);
            $workTitle = $work->title;
        } else {
            $workTitle = '';
        }
        if(isset($profileInfo['cover_pic'])){
            $cover_photo = '/uploads/images/'.$profileInfo['cover_pic']['v'];
        } else {
            $cover_photo = '/defaultMedia/false_cover.png';
        }*/
        $friends = new FriendRepository(new Friend(),new FriendRequest(), new FriendGroup());
        $relation = $friends->userRelation($userId,$ref_id);
        $status_creator = $friends->isStatusCreator($userId,$ref_id);
        $rv['fr_status'] = $relation['status'];
        $rv['status_creator'] = $status_creator;
        $rv['fr_privacy'] = $relation['privacy'];
        $rv['follow_status'] = $friends->isFollowing($userId,$ref_id);
        $rv['cover_photo'] = $cover_photo;
        $rv['workTitle'] = $workTitle;
        $rv['country'] = $country;

        return $rv;
    }

    public function getUserTypeById($userId) {
        return $this->profileModel->where(['user_id'=> $userId, 'key'=>'user_type'])->first(['key','value']);
    }

    public function isProfileProfessional($userId) {
        return ($this->getUserTypeById($userId)->value === "2") ? true : false;
    }


    public function getExpertise($id=null) {
        if(!is_null($id)) {
            return Expertise::find($id);
        }
        return Expertise::all();
    }

    public function getExpertiseArea($expertise_id=null,$id=null) {
        if(!is_null($expertise_id) && !is_null($id)) {
            return ExpertiseArea::where(['expertise_id'=> $expertise_id, 'id'=>$id])->get();
        }
        else if(!is_null($expertise_id) && is_null($id)) {
            return ExpertiseArea::where('expertise_id',$expertise_id)->get();
        }
        else if(is_null($expertise_id) && !is_null($id)) {
            return ExpertiseArea::find($id);
        }
        return ExpertiseArea::all();
    }

    public function getUserHomeTown($useId) {
        return $this->profileModel->where(['user_id'=>$useId, 'key'=>'home_town'])->get();
    }

    public function getUserCountry($useId) {
        return $this->profileModel->where(['user_id'=>$useId, 'key'=>'country'])->get();
    }
}
