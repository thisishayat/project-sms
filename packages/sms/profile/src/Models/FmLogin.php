<?php

namespace Freemig\Profile\Models;

use Illuminate\Database\Eloquent\Model;

class FmLogin extends Model
{
    protected $table = 'fm_login';
    public $timestamps = false;
}
