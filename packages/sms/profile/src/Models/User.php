<?php

namespace Freemig\Profile\Models;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Cmgmyr\Messenger\Traits\Messagable;
class User extends EloquentModel implements Authenticatable
{
    protected $table = 'users';
    use Messagable;
    public function getAuthIdentifierName(){

    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier(){

    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword(){

    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken(){

    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value){

    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName(){

    }
}
