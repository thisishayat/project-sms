<?php

namespace Freemig\Profile\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertiseArea extends Model
{
    protected $table = 'expertise_area';
}
