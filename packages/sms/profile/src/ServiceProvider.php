<?php namespace Sms\Profile;
use Sms\Profile\Helpers;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected $packageName = "profile";

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {

        $this->handleConfigs();
        $this->handleMigrations();
        $this->handleViews();
        $this->handleTranslations();
        $this->handleRoutes();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {

        // Bind any implementations.
        include __DIR__.'/routes.php';

        $this->app->bind('ProfileHelper', function()
        {
            return new ProfileHelper();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {

        return [];
    }

    private function handleConfigs() {

        $configPath = __DIR__ . '/../config/'.$this->packageName.'.php';

        $this->publishes([$configPath => config_path($this->packageName.'.php')]);

        $this->mergeConfigFrom($configPath, $this->packageName);
    }

    private function handleTranslations() {

        $this->loadTranslationsFrom(__DIR__.'/../lang', $this->packageName);
    }

    private function handleViews() {

        $this->loadViewsFrom(__DIR__.'/../views', $this->packageName);

        $this->publishes([__DIR__.'/../views' => base_path('resources/views/vendor/'.$this->packageName)]);
    }

    private function handleMigrations() {

        $this->publishes([__DIR__ . '/../migrations' => base_path('database/migrations')]);
    }

    private function handleRoutes() {

        include __DIR__.'/../routes.php';
    }
}
